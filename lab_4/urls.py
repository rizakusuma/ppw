"""myweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.http import HttpResponse
from django.urls import path

from . import views
from lab_4 import views as appViews

app_name = 'lab_4'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', appViews.index),
    url(r'^about/', appViews.about),
    url(r'^contact/', appViews.contact),
    url(r'^registration/', appViews.registration),
    url(r'^schedule/delete/', appViews.delete, name="delete"),
    url(r'^schedule/', appViews.schedule),
    url(r'^status/delete1/', appViews.delete1, name="delete1"),
    url(r'^status/', appViews.status),
    url(r'^books/$', appViews.book),
    url(r'^$', appViews.index),
    url(r'^books/api/books/(?P<search>\w+)/$', appViews.book_data),
    url(r'^subscribe/$', appViews.Subscribe_Views),
    url(r'^subscribe/check_email/$', appViews.check_email),
    # url(r'^get-subs-list/$', appViews.subs_list_json),
    # url(r'^subscribe/delete/$', appViews.unsubscribe),

]

