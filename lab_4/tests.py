from django.test import TestCase, Client
from django.urls import resolve
from .views import index, about, registration, contact, schedule, status, book, book_data, Subscribe_Views, check_email
from .models import Status, Schedule, Subscribe
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time


class Story6test(TestCase):
    def test_lab_3_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_to_do_list_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'Status.html')

    def test_lab_3_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_model_can_create_new_activity(self):
            #Creating a new activity
            new_activity = Status.objects.create(judul='Aku mau latihan ngoding deh', isiJudul ='mau latihan ngoding juga')

            #Retrieving all available activity
            counting_all_available_activity = Status.objects.all().count()
            self.assertEqual(counting_all_available_activity,1)
    def test_model_can_create_new_schedule(self):
            #Creating a new activity
            new_activity = Schedule.objects.create(judul='Aku mau latihan ngoding deh', day ='2000-09-10', time= '09:10 AM', activity='halo', place='haii', category='hehe')

            #Retrieving all available activity
            counting_all_available_activity = Schedule.objects.all().count()
            self.assertEqual(counting_all_available_activity,1)

    def test_delete2(self):
        new_delete = Schedule.objects.create(judul='Aku mau latihan ngoding deh', day ='2000-09-10', time= '09:10 AM', activity='halo', place='haii', category='hehe')
        Client().get('/delete')
        self.assertEqual(Schedule.objects.all().count(),1)


    def test_delete(self):
        new_delete = Status.objects.create(judul='coba delete', isiJudul = 'langsung delete')
        Client().get('/delete')
        self.assertEqual(Status.objects.all().count(),1)

    def test_create_anything_model(self, judul='ini judul', isiJudul='ini isi judul'):
        return Status.objects.create(judul= judul, isiJudul=isiJudul)
    
    def test_lab_6_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_profile_tempate(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'About.html')

    def test_lab6_using_index_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)


    def test_lab_6_index_is_exist(self):
        response = Client().get('/index/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_index_template(self):
        response = Client().get('/index/')
        self.assertTemplateUsed(response, 'Home.html')

    def test_lab6_using_index_func(self):
        found = resolve('/index/')
        self.assertEqual(found.func, index)

    def test_lab_6_contact_is_exist(self):
        response = Client().get('/contact/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_contact_template(self):
        response = Client().get('/contact/')
        self.assertTemplateUsed(response, 'Contact.html')

    def test_lab6_using_contact_func(self):
        found = resolve('/contact/')
        self.assertEqual(found.func, contact)


    def test_lab_6_registration_is_exist(self):
        response = Client().get('/registration/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_registration_template(self):
        response = Client().get('/registration/')
        self.assertTemplateUsed(response, 'Registration.html')

    def test_lab6_using_registration_func(self):
        found = resolve('/registration/')
        self.assertEqual(found.func, registration)       

    def test_lab_6_schedule_is_exist(self):
        response = Client().get('/schedule/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_schedule_template(self):
        response = Client().get('/schedule/')
        self.assertTemplateUsed(response, 'Schedule.html')

    def test_lab6_using_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_lab_6_status_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_lab6_url_status_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'Status.html')

    def test_lab6_using_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)      

    def test_can_save_a_POST_request(self):
        response = self.client.post('/status/', data={'judul': 'mau belajar', 'isiJudul' : 'abis belajar main'})
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response['status'], '/')

        new_response = self.client.get('/status/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('mau belajar', html_response)

    def test_lab5_post_success_and_render_the_resul(self):
        test = 'anonymous'
        response_post = Client().post('/status/', {'judul': 'test', 'isiJudul': 'test'})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_Story9_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_Story9_using_index_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'Books.html')

    def test_Story9_using_Book_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, book)

    def test_8Story10_using_index_template(self):
        response = Client().get('/subscribe/')
        self.assertTemplateUsed(response, 'Subscribe.html')

    def test_Story_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func,index)
    
    # def test_Story10_using_func(self):
    #     found = resolve('/subscribe/')
    #     self.assertEqual(found.func, Subscribe_Views)    

    def test_Story10_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code,200)

    def Story10_test_model_can_create_new_activity(self):
            #Creating a new activity
        new_activity = Subscribe.objects.create(email='jija@gmail.com', nama ='riza', password1 = 'A1b2c3d.')

            #Retrieving all available activity
        counting_all_available_activity = Subscribe.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)


# class NewVisitorTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('-dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']

#         self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
#         self.browser.implicitly_wait(3)
#         super(NewVisitorTest,self).setUp()


#     def test_can_fill_itself(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         time.sleep(3) # Let the user actually see something
#         input_box = self.browser.find_element_by_id('id_judul')
#         input_box2 = self.browser.find_element_by_id('id_isiJudul')
#         input_box.send_keys('Halo nama saya jija')
#         input_box2.send_keys('saya orang ganteng')
#         input_box.submit()
#         time.sleep(3)
#         self.assertIn("jija", self.browser.page_source)

#     def test_css_for_form(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         form = self.browser.find_element_by_tag_name('form')
#         self.assertIn('text-white', form.get_attribute('class'))

#     def test_css_for_title(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         title = self.browser.find_element_by_tag_name('title')
#         self.assertIn('Landpage', title.get_attribute('innerHTML'))

#     def test_css_for_background_color_h1(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         header = self.browser.find_element_by_tag_name('h1').value_of_css_property('background-color')
#         self.assertIn(header, 'rgba(30, 144, 255, 1)')

#     def test_css_for_font_color_h1(self):
#         self.browser.get('http://muhlab6.herokuapp.com/')
#         header2 = self.browser.find_element_by_tag_name('h1').value_of_css_property('color')
#         self.assertIn(header2, 'rgba(255, 255, 255, 1)')



if __name__ == '__main__':
    unittest.main(warnings='ignore')