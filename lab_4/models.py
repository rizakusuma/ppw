from django.db import models
from django.utils import timezone
from datetime import datetime

class Schedule(models.Model):
	judul = models.CharField(max_length = 50)
	day = models.DateField();
	time = models.TimeField();
	activity = models.CharField(max_length = 50)
	place = models.CharField(max_length = 50)
	category = models.CharField(max_length = 50)
	
	def __str__(self):
		return "{}".format(self.judul)

class Status(models.Model):
	judul = models.CharField(max_length = 300)
	isiJudul = models.CharField(max_length = 300)
	def __str__(self):
		return "{}".format(self.judul)

class Subscribe(models.Model):
	email = models.EmailField()
	nama = models.CharField(max_length=150)
	password1 = models.CharField(max_length=100)

	def as_dict(self):
		return{
			"email" : self.email,
			"nama" : self.nama,
			"password1" : self.password1,
		}

	def __str__(self):
		return "{}".format(self.email)


