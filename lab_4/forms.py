from django import forms
from django.forms import ModelForm
from lab_4.models import Schedule
from lab_4.models import Status
from lab_4.models import Subscribe

class scheduleForm(forms.ModelForm):
	class Meta:
		model = Schedule
		fields = ['judul','day','time','activity','place', 'category']
		widgets = {
			'judul': forms.TextInput(attrs={'class' : 'form-control'}),
			'day': forms.TextInput(attrs={'type':'date','class' : 'form-control'}),
			'time': forms.TextInput(attrs={'type':'time', 'class': 'form-control'}),
			'activity': forms.TextInput(attrs={'class': 'form-control'}),
			'place': forms.TextInput(attrs={'class': 'form-control'}),
			'category': forms.TextInput(attrs={'class': 'form-control'}),
		}
		label = {
			'judul': 'Judul',
			'day': 'Day',
			'time': 'Time',
			'activity': 'Activity',
			'place': 'Place'
		}

class statusForm(forms.ModelForm):
	class Meta:
		model = Status
		fields = ['judul','isiJudul']
		widgets = {
			'judul': forms.TextInput(attrs={'class' : 'form-control'}),
			'isiJudul': forms.TextInput(attrs={'class' : 'form-control'}),
		}

		label = {
			'judul': 'Judul',
			'isiJudul': 'Isi Judul'
		}


class subscribeForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Subscribe
        fields = ["email","nama","password1"]
