from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Schedule
from .forms import scheduleForm
from .models import Status
from .forms import statusForm
from .models import Subscribe
from .forms import subscribeForm
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import urllib.request, json
import requests
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

def index(request):
    return render(request, 'Home.html')

def about(request):
    return render(request, 'About.html')

def contact(request):
    return render(request, 'Contact.html')

def registration(request):
    return render(request, 'Registration.html')

def schedule(request):
	posted = Schedule.objects.all()
	if request.method == 'POST':
		form = scheduleForm(request.POST)
		if form.is_valid():

			form.save()
			form = scheduleForm()

			context = {
				'Posts' : posted,
				'scheduleForm' : form,
			}
			return render(request, 'Schedule.html', context)

	else :
		
		form = scheduleForm()
		context = {
		'Posts': posted,
		'scheduleForm' : form,
		}
	return render(request,'Schedule.html', context)

def status(request):
	posted = Status.objects.all()
	if request.method == 'POST':
		form = statusForm(request.POST)
		if form.is_valid():

			form.save()
			form = statusForm()

			context = {
				'Posts' : posted,
				'statusForm' : form,
			}
			return render(request, 'Status.html', context)

	else :
		
		form = statusForm()
		context = {
		'Posts': posted,
		'statusForm' : form,
		}
	return render(request,'Status.html', context)

def delete(request):
	s= Schedule.objects.all().delete()
	print("Sdsiofljshduof")
	print(s)
	return redirect('/schedule')


def delete1(request):
	t= Status.objects.all().delete()
	print("Sdsiofljshduof")
	print(t)
	return redirect('/status')


def book_data(request,search):

	raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ search).json()
	return JsonResponse(raw_data)

def book(request):
	return render(request, 'Books.html')

@csrf_exempt
def Subscribe_Views(request):
	all_subs = Subscribe.objects.all()
	subs_list = Subscribe.objects.all()
	if request.method == "POST":
		form = subscribeForm(request.POST)
		if form.is_valid():
			form_item = form.save(commit=True)
			form_item.save()
			form = subscribeForm()
	else:
		form = subscribeForm()
		
	return render(request, 'Subscribe.html', {'form' : form, 'subs_list' : subs_list})

@csrf_exempt
def check_email(request):
	if request.method == 'POST':
		email = request.POST['email']
		nama = request.POST['nama']
		password1 = request.POST['password1']

		subs_filter = Subscribe.objects.filter(email = email)

		if len(subs_filter) > 0 :
			return JsonResponse({'message' : 'Email already used'})

		else:
			return JsonResponse({'message' : 'User Valid'})

	else:
		return JsonResponse({'message' : 'Something went wrong'})